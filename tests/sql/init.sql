BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SAVEPOINT error_not_initialized;
SELECT faker.name();
ROLLBACK TO SAVEPOINT error_not_initialized;

SELECT faker.faker();
SELECT faker.faker(ARRAY['de_DE','jp_JP']);
SELECT faker.faker('fr_FR');

SELECT pg_typeof(faker.name()) = 'TEXT'::REGTYPE;

SELECT count(DISTINCT name) FROM faker._functions();

ROLLBACK;
