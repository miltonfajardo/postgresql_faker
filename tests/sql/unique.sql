BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.faker();
SELECT faker.seed(4321);

SELECT faker.unique_null_boolean();
SELECT faker.unique_null_boolean();
SELECT faker.unique_null_boolean();

SAVEPOINT before_uniqueness_error;
SELECT faker.unique_null_boolean();
ROLLBACK TO SAVEPOINT before_uniqueness_error;

SELECT faker.unique_clear();

SELECT faker.unique_null_boolean();

ROLLBACK;
