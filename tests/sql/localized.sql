BEGIN;

CREATE SCHEMA faker;
CREATE EXTENSION faker SCHEMA faker CASCADE;

SELECT faker.faker('fr_FR');

SELECT faker.department() IS NOT NULL;

SAVEPOINT before_attribute_error;
SELECT faker.military_apo() IS NOT NULL;
ROLLBACK TO before_attribute_error;

SELECT faker.faker('en_US');
SELECT faker.military_apo() IS NOT NULL;

ROLLBACK;
