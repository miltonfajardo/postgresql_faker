# /usr/bin/python3
#
# Usage: ./providers.py
#
# - This is a python script that uses a Jinja2 template to createa SQL script.
# - The SQL script contains pl/python functions.
# - The pl/python functions map the providers methods of the faker library.
#

import sys
from typing import Dict
from types import ModuleType, MethodType
import faker  # type: ignore
import inspect
import pkgutil
from jinja2 import Environment, FileSystemLoader

pg_type = {
    'bool': 'BOOLEAN',
    'float': 'FLOAT',
    'str': 'TEXT',
    'int': 'INTEGER',
    'NoneType': 'TEXT',
    'type': 'TEXT',
    'tuple': 'TEXT[]'
}

excluded_standard_providers = [
    'BaseProvider',
    'OrderedDict',
    'choices_distribution',
    'choices_distribution_unique',
    're',
    'string'
]

excluded_localized_providers = [
    'fr_QC'     # fr_QC locale is deprecated. Use fr_CA instead.
]

# some python Faker function cannot be declared with the same name because
# that's a reserved name
excluded_methods = [
    'boolean',  # pg reserved keyword
    'time',     # pg reserved keyword
    'uuid4',    # bug
    'dsv',      # JSON
    'csv',      # JSON
    'tsv',      # JSON
    'psv',      # JSON
]


# We loop over all the elements of a Faker provider module
# and we keep only the methods we want to map in pl/python
def getMethods(module_object: ModuleType) -> Dict:
    result: Dict = {}
    for method_name in module_object.__dict__.keys():
        method_object = getattr(module_object, method_name)
        # ignore some elements of the module
        if (
            not callable(method_object)
            or method_name[0] == '_'
            or method_name in excluded_methods
        ):
            continue
        result[method_name] = {
            'name': method_name,
            'type': 'TEXT',  # we can't guess the return type of the method
            'parameters': getParameters(method_object)
        }
    return result


# We loop over all the parameters of a Faker method
# we keep only the useful ones
# we extract the default value
# and try to infer the data type (would be nice if Faker had annotations ;-) )
def getParameters(method_object: MethodType) -> Dict:
    result: Dict = {}
    parameters = inspect.signature(method_object).parameters
    for p in parameters:
        # ignore the self parameter
        if p == 'self':
            continue
        # fetch the default value and guess the type
        param_default = parameters[p].default
        param_type = pg_type[type(param_default).__name__]
        # Special case: when the defaut value is empty
        if param_default == inspect._empty:
            param_default = ''
            param_type = 'TEXT'
        # Special case: the default value is a tuple
        if param_default == ():
            param_default = 'ARRAY[]::TEXT[]'
            param_type = 'TEXT[]'
        # Special case: surround text value with quotes
        if param_type == 'TEXT':
            param_default = "'%s'" % param_default
        # Special case: `None` means `NULL`
        if param_default in ["None", "'None'"]:
            param_default = "NULL"
        # append to the result
        result[p] = {
            'name': p,
            'default': param_default,
            'type': param_type
        }
    return result


#
# Parse the Faker module
#
r: Dict = {}

# loop over the standard providers
for s_importer, s_modname, s_ispkg in (
    pkgutil.iter_modules(faker.providers.__path__)
):
    # ignore some providers
    if (
        not s_ispkg
        or s_modname in excluded_standard_providers
        or s_modname[0] == '_'
    ):
        continue

    # Import methods from the standard provider
    # Example : faker.providers.address.Provider
    standard_provider = 'faker.providers.%s' % s_modname
    r[standard_provider] = getMethods(sys.modules[standard_provider].Provider)

    # Loop of the localized providers of the standard provider
    # Some localized providers extend the standard provider with their own
    # methods
    for l_importer, l_modname, l_ispkg in (
        pkgutil.iter_modules(sys.modules[standard_provider].__path__)
    ):

        # exclude some localized providers
        if (
            not l_ispkg
            or l_modname in excluded_localized_providers
        ):
            continue

        # Import methods from the localized provider
        # example: faker.providers.address.fr_FR.Provider
        localized_provider = 'faker.providers.%s.%s' % (s_modname, l_modname)

        # Load once the localized provider to get its methods
        # /!\ We should probably use importlib in `getMethods()` and avoid this
        if localized_provider not in sys.modules:
            faker.Faker(l_modname)

        # Even when loaded, some providers don't appear in sys.modules
        if localized_provider in sys.modules:
            r[localized_provider] = getMethods(
                                    sys.modules[localized_provider].Provider
                                    )

#
# Output
#
env = Environment(loader=FileSystemLoader('./faker/'))
template = env.get_template('providers.sql.j2')
print(template.render(r=r))
