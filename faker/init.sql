
-- initialize the faker engine
-- usage: SELECT faker.faker(ARRAY('fr_FR','ja_JP'))
CREATE OR REPLACE FUNCTION faker(
  locales TEXT[]
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locales)
  return True
$$
  LANGUAGE plpython3u
;

--
-- initialize the faker engine with one or zero locale
-- usage:
--  - SELECT faker.faker()
--  - SELECT faker.faker('de_DE')
--
CREATE OR REPLACE FUNCTION faker(
  locale TEXT DEFAULT 'en_US'
)
RETURNS BOOLEAN
AS $$
  from faker import Faker
  GD['Faker'] = Faker(locale)
  return True
$$
  LANGUAGE plpython3u
;

--
-- Seeding the generator
-- https://github.com/joke2k/faker#seeding-the-generator
--
CREATE OR REPLACE FUNCTION seed(
  seed TEXT
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.error("faker is not initialized.",
                hint="Use SELECT faker.faker(); first.")
  return True
$$
  LANGUAGE plpython3u
;

CREATE OR REPLACE FUNCTION seed(
  seed INTEGER
)
RETURNS BOOLEAN
AS $$
  try:
    GD['Faker'].seed_instance(seed)
  except KeyError:
    plpy.error("faker is not initialized.",
                hint="Use SELECT faker.faker(); first.")
  return True
$$
  LANGUAGE plpython3u
;


