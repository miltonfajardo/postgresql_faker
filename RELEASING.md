RELEASING
================================================================================


* [ ] Check that **all** CI jobs run without errors on the `master` branch

* [ ] Close all remaining issues on the current milestone

* [ ] Update the [Changelog]

* [ ] Add the sql file

* [ ] Upload the zipball to PGXN

* [ ] Check the PGXN install process

* [ ] Close the current milsetone and open the next one

* [ ] Tag the master branch

* [ ] Open a ticket to the [PostgreSQL RPM repository project]

* [ ] Bump to the new version number in [anon.control] and [META.json]

[Changelog]: CHANGELOG.md
[NEWS.md]: NEWS.md
[anon.control]: anon.control
[META.json]: META.json
[PostgreSQL RPM repository project]: https://redmine.postgresql.org/projects/pgrpms/issues
